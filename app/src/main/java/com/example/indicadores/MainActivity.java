package com.example.indicadores;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private TextView Tvchillan;
    private TextView Tvmoscu;
    private TextView Tvparis;
    private TextView Tvmadrid;
    private TextView TvtempCh;
    private TextView TvhumeCh;
    private TextView TvpresionCh;
    private TextView TvtempMs;
    private TextView TvhumeMs;
    private TextView TvpresionMs;
    private TextView TvtempPa;
    private TextView TvhumePa;
    private TextView TvpresionPa;
    private TextView TvtempMd;
    private TextView TvhumeMd;
    private TextView TvpresionMd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.Tvchillan = (TextView) findViewById(R.id.Tvchillan);
        this.Tvmoscu = (TextView) findViewById(R.id.Tvmoscu);
        this.Tvparis = (TextView) findViewById(R.id.Tvparis);
        this.Tvmadrid = (TextView) findViewById(R.id.Tvmadrid);
        this.TvtempCh = (TextView) findViewById(R.id.TvtempCh);
        this.TvhumeCh = (TextView) findViewById(R.id.TvhumeCh);
        this.TvpresionCh = (TextView) findViewById(R.id.TvpresionCh);
        this.TvtempMs = (TextView) findViewById(R.id.TvtempMs);
        this.TvhumeMs = (TextView) findViewById(R.id.TvhumeMs);
        this.TvpresionMs = (TextView) findViewById(R.id.TvpresionMs);
        this.TvtempMd = (TextView) findViewById(R.id.TvtempMd);
        this.TvhumeMd = (TextView) findViewById(R.id.TvhumeMd);
        this.TvpresionMd = (TextView) findViewById(R.id.TvpresionMd);
        this.TvtempPa = (TextView) findViewById(R.id.TvtempPa);
        this.TvhumePa = (TextView) findViewById(R.id.TvhumePa);
        this.TvpresionPa = (TextView) findViewById(R.id.TvpresionPa);



        String urlCh = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6&lon=-72.1167&appid=2ec8c33c02bae39e3292f3a80d6f490b&units=metric";
        StringRequest solicitudCh = new StringRequest(

                Request.Method.GET,
                urlCh,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {


                            JSONObject jsonObject = new JSONObject(response);

                            JSONObject weatherData = new JSONObject(jsonObject.getString("main"));



                            double tempInt = Double.parseDouble(weatherData.getString("temp"));
                            double centi = tempInt;
                            centi = Math.round(centi);
                            int i = (int)centi;
                            double humidity = Double.parseDouble(weatherData.getString("humidity"));
                            double pressure = Double.parseDouble(weatherData.getString("pressure"));






                            TvtempCh.setText("Temperatura " + i + "°");
                            TvhumeCh.setText("Humedad "+humidity + "%");
                            TvpresionCh.setText("Presión " + pressure + "hPa");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }





                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        String urlMd = "http://api.openweathermap.org/data/2.5/weather?lat=40.4167&lon=-3.70325&appid=2ec8c33c02bae39e3292f3a80d6f490b&units=metric";
        StringRequest solicitudMd = new StringRequest(

                Request.Method.GET,
                urlMd,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {


                            JSONObject jsonObject = new JSONObject(response);

                            JSONObject weatherData = new JSONObject(jsonObject.getString("main"));



                            double tempInt = Double.parseDouble(weatherData.getString("temp"));
                            double centi = tempInt;
                            centi = Math.round(centi);
                            int i = (int)centi;
                            double humidity = Double.parseDouble(weatherData.getString("humidity"));
                            double pressure = Double.parseDouble(weatherData.getString("pressure"));






                            TvtempMd.setText("Temperatura " + i + "°");
                            TvhumeMd.setText("Humedad "+ humidity + "%");
                            TvpresionMd.setText("Presión " + pressure + "hPa");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }





                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }


        );

        String urlPa = "http://api.openweathermap.org/data/2.5/weather?lat=48.8534000&lon=2.3486000&appid=2ec8c33c02bae39e3292f3a80d6f490b&units=metric";
        StringRequest solicitudPa = new StringRequest(

                Request.Method.GET,
                urlPa,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {


                            JSONObject jsonObject = new JSONObject(response);

                            JSONObject weatherData = new JSONObject(jsonObject.getString("main"));



                            double tempInt = Double.parseDouble(weatherData.getString("temp"));
                            double centi = tempInt;
                            centi = Math.round(centi);
                            int i = (int)centi;
                            double humidity = Double.parseDouble(weatherData.getString("humidity"));
                            double pressure = Double.parseDouble(weatherData.getString("pressure"));






                            TvtempPa.setText("Temperatura " + i + "°");
                            TvhumePa.setText("Humedad "+ humidity + "%");
                            TvpresionPa.setText("Presión " + pressure + "hPa");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }





                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }


        );

        String urlMs = "http://api.openweathermap.org/data/2.5/weather?lat=55.7522202&lon=37.6155586&appid=2ec8c33c02bae39e3292f3a80d6f490b&units=metric";
        StringRequest solicitudMs = new StringRequest(

                Request.Method.GET,
                urlMs,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {


                            JSONObject jsonObject = new JSONObject(response);

                            JSONObject weatherData = new JSONObject(jsonObject.getString("main"));



                            double tempInt = Double.parseDouble(weatherData.getString("temp"));
                            double centi = tempInt;
                            centi = Math.round(centi);
                            int i = (int)centi;
                            double humidity = Double.parseDouble(weatherData.getString("humidity"));
                            double pressure = Double.parseDouble(weatherData.getString("pressure"));






                            TvtempMs.setText("Temperatura " + i + "°");
                            TvhumeMs.setText("Humedad "+ humidity + "%");
                            TvpresionMs.setText("Presión " + pressure + "hPa");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }





                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }


        );











        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(solicitudCh);
        listaEspera.add(solicitudMs);
        listaEspera.add(solicitudMd);
        listaEspera.add(solicitudPa);
    }
}
